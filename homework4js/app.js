let num1 = prompt("Введіть перше число:");
let num2 = prompt("Введіть друге число:");
let operator = prompt("Введіть операцію (+, -, *, /):");

function calculate(num1, num2, operator) {
    let result;
    switch (operator) {
        case "+":
            result = Number(num1) + Number(num2);
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        default:
            result = "Некоректна операція!";
    }
    return result

}
const result = calculate(num1 , num2 , operator)

console.log(result)