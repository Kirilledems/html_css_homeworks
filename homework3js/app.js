const userInput = prompt("Введіть число:");
const number = parseInt(userInput);
if (isNaN(number)) {
    console.log("Введено не число");
} else {
    let found = false;
    for (let i = 0; i <= number; i++) {
        if (i % 5 === 0) {
            console.log(i);
            found = true;
        }
    }
    if (found) {
        console.log("Sorry, no numbers");
    }
}